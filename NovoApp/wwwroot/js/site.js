﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$(document).ready(function () {
    $.ajaxSetup({ cache: false });
});

function RenderActions(RenderActionstring) {
    $("#OpenDialog").load(RenderActionstring);
};



function CreateNew() {
    if (!ValidateInputs())
        return;
    $("#loader").show();

    $.ajax({
        url: '/Tasks/Create/',
        type: 'POST',
        data: $("#CreateForm").serialize(),
        success: function () {
            $("#loader").hide();
            $("#modalCreate").modal("hide");
            swal("Success", "Task Created", "success");
            window.location.reload();
        }
    })
};

function EditEmp(id) {
    $("#loader").show();
    $.ajax({
        url: '/Tasks/Edit/' + id,
        type: 'POST',
        data: $('#EditForm').serialize(),
        success: function () {
            $("#loader").hide();
            $("#modalCreate").modal("hide");
            swal("Success", "Task Edited", "success");
            window.location.reload();
        }
     
    })
};


function CreateTask() {
    if (!ValidateInput())
        return;
    $("#loader").show();
    $.ajax({
        url: '/Requests/Create/',
        type: 'POST',
        data: $("#CreateF").serialize(),
        success: function () {
            $("#loader").hide();
            $("#modalCreate").modal("hide");
            swal("Success", "Request Created", "success");
            window.location.reload();
        }
    })
};

function EditT(id) {
    $("#loader").show();
    $.ajax({
        url: '/Requests/Edit/' + id,
        type: 'POST',
        data: $('#EditR').serialize(),
        success: function () {
            $("#loader").hide();
            $("#modalCreate").modal("hide");
            swal("Success", "Request Edited", "success");
            window.location.reload();
        }

    })
};


function ValidateInputs() {
    var flag = true;
    var firstNameInput = $('#TaskName');
    var secondNameInput = $('#TaskDescription');
   

    if ($.trim(firstNameInput.val()) != '') {
        firstNameInput.closest('.form-group').find('span').hide();
        flag = true;
    }

    if ($.trim(secondNameInput.val()) != '') {
        secondNameInput.closest('.form-group').find('span').hide();
        flag = true;
    }

    if ($.trim(firstNameInput.val()) === '') {
        firstNameInput.closest('.form-group').find('span').text("This field is required");
        flag = false;
    }

    if ($.trim(secondNameInput.val()) === '') {
        secondNameInput.closest('.form-group').find('span').text("This field is required");
        flag = false;
    }

    return flag;
};


function ValidateInput() {
    var flag = true;
    var firstNameInput = $('#RequestName');
    var secondNameInput = $('#RequestDescription');


    if ($.trim(firstNameInput.val()) != '') {
        firstNameInput.closest('.form-group').find('span').hide();
        flag = true;
    }

    if ($.trim(secondNameInput.val()) != '') {
        secondNameInput.closest('.form-group').find('span').hide();
        flag = true;
    }

    if ($.trim(firstNameInput.val()) === '') {
        firstNameInput.closest('.form-group').find('span').text("This field is required");
        flag = false;
    }

    if ($.trim(secondNameInput.val()) === '') {
        secondNameInput.closest('.form-group').find('span').text("This field is required");
        flag = false;
    }

    return flag;
};