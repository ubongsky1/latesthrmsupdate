﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Novo.Models.Entity;
using NovoClients.DataAccess.Data;

namespace NovoApp.Controllers
{
    public class HandoverLeavesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public HandoverLeavesController(ApplicationDbContext context,
                                                   SignInManager<ApplicationUser> signInManager,
                                                   UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _signInManager = signInManager;
            _userManager = userManager;
        }

        // GET: HandoverLeaves
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.HandoverLeaves.Include(h => h.GetUser);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: HandoverLeaves/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var handoverLeave = await _context.HandoverLeaves
                .Include(h => h.GetUser)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (handoverLeave == null)
            {
                return NotFound();
            }

            return View(handoverLeave);
        }

        // GET: HandoverLeaves/Create
        public IActionResult Create()
        {
            ViewData["EmployeeId"] = new SelectList(_context.Users, "Id", "FirstName");
            return View();
        }

        // POST: HandoverLeaves/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Employee,Note,EmployeeId,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete")] HandoverLeave handoverLeave)
        {
            if (ModelState.IsValid)
            {
                _context.Add(handoverLeave);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["EmployeeId"] = new SelectList(_context.Users, "Id", "FirstName", handoverLeave.EmployeeId);
            return View(handoverLeave);
        }

        // GET: HandoverLeaves/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var handoverLeave = await _context.HandoverLeaves.FindAsync(id);
            if (handoverLeave == null)
            {
                return NotFound();
            }
            ViewData["EmployeeId"] = new SelectList(_context.Users, "Id", "FirstName", handoverLeave.EmployeeId);
            return View(handoverLeave);
        }

        // POST: HandoverLeaves/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Employee,Note,EmployeeId,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete")] HandoverLeave handoverLeave)
        {
            if (id != handoverLeave.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(handoverLeave);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HandoverLeaveExists(handoverLeave.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EmployeeId"] = new SelectList(_context.Users, "Id", "FirstName", handoverLeave.EmployeeId);
            return View(handoverLeave);
        }

        // GET: HandoverLeaves/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var handoverLeave = await _context.HandoverLeaves
                .Include(h => h.GetUser)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (handoverLeave == null)
            {
                return NotFound();
            }

            return View(handoverLeave);
        }

        // POST: HandoverLeaves/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var handoverLeave = await _context.HandoverLeaves.FindAsync(id);
            _context.HandoverLeaves.Remove(handoverLeave);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HandoverLeaveExists(int id)
        {
            return _context.HandoverLeaves.Any(e => e.Id == id);
        }
    }
}
