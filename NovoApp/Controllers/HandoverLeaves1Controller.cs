﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Novo.Models.Entity;
using NovoClients.DataAccess.Data;
using SmtpClient = MailKit.Net.Smtp.SmtpClient;
using MimeKit;
using MimeKit.Text;

namespace NovoApp.Controllers
{
    public class HandoverLeaves1Controller : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public HandoverLeaves1Controller(ApplicationDbContext context,
                                           SignInManager<ApplicationUser> signInManager,
                                             UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _signInManager = signInManager;
            _userManager = userManager;
        }

        // GET: HandoverLeaves1
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.HandoverLeaves.Include(h => h.GetUser);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: HandoverLeaves1/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var handoverLeave = await _context.HandoverLeaves
                .Include(h => h.GetUser)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (handoverLeave == null)
            {
                return NotFound();
            }

            return View(handoverLeave);
        }

        // GET: HandoverLeaves1/Create
        public IActionResult Create()
        {
            var user = (from u in _context.Users
                        join b in _context.UserRoles on u.Id equals b.UserId
                        join c in _context.Roles on b.RoleId equals c.Id
                        where u.Id == b.UserId && c.Name != "User"
                        select new
                        {
                            Value = b.UserId,
                            Text = u.FirstName + ' ' + u.LastName
                        }).ToList();
                
            ViewData["EmployeeId"] = new SelectList(user, "Value", "Text");
            return View();
        }

        // POST: HandoverLeaves1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Employee,Note,EmployeeId,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete")] HandoverLeave handoverLeave)
        {
            if (ModelState.IsValid)
            {
                _context.Add(handoverLeave);
                await _context.SaveChangesAsync();

                var user = await _userManager.GetUserAsync(User);
                var check = _context.Users.Where(x => x.Id == handoverLeave.EmployeeId).Select(s => new { s.Email }).FirstOrDefault();
                var samps = check.Email;

                var message = new MimeMessage();
                //Setting the To e-mail address
                message.To.Add(new MailboxAddress(samps));


                var tt = "akinbamidelea@novohealthafrica.org";
                message.Bcc.Add(new MailboxAddress(tt));
                //Setting the From e-mail address
                message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                //E-mail subject 
                message.Subject = "Handover Leaves";
                //E-mail message body
                message.Body = new TextPart(TextFormat.Html)
                {
                    Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                   "<br/><br/><br/><br/>" +
                "<p>Dear Colleague,</p>" +

               "<p> This is to notify that handover has been made by " + user.FirstName + " " + user.LastName +

               "<br/>on </a>" + handoverLeave.CreateDate + ".</p>" +
               "You can see more information on the handover by Clicking <a href='https://hrms.novohealthafrica.org/HandoverLeaves1/Details/" + handoverLeave.Id + "'>Here</a>" +




                                              "<br/>Regards<br/>" +


                                               "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"
                };

                //Configure the e-mail
                using (var emailClient = new SmtpClient())
                {

                    emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                    emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                    emailClient.Connect("smtp.office365.com", 587, false);
                    emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2009aa");
                    emailClient.Send(message);
                    emailClient.Disconnect(true);
                }

                return RedirectToAction(nameof(Index));
            }
            var test = (from u in _context.Users
                        join b in _context.UserRoles on u.Id equals b.UserId
                        join c in _context.Roles on b.RoleId equals c.Id
                        where u.Id == b.UserId && c.Name != "User"
                        select new
                        {
                            Value = b.UserId,
                            Text = u.FirstName + ' ' + u.LastName
                        }).ToList();

            ViewData["EmployeeId"] = new SelectList(test, "Value", "Text", handoverLeave.EmployeeId);
            //ViewData["EmployeeId"] = new SelectList(_context.Users, "Id", "FirstName", handoverLeave.EmployeeId);
            return View(handoverLeave);
        }

        // GET: HandoverLeaves1/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var handoverLeave = await _context.HandoverLeaves.FindAsync(id);
            if (handoverLeave == null)
            {
                return NotFound();
            }
            ViewData["EmployeeId"] = new SelectList(_context.Users, "Id", "FirstName", handoverLeave.EmployeeId);
            return View(handoverLeave);
        }

        // POST: HandoverLeaves1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Employee,Note,EmployeeId,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete")] HandoverLeave handoverLeave)
        {
            if (id != handoverLeave.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(handoverLeave);
                    await _context.SaveChangesAsync();
                    var user = await _userManager.GetUserAsync(User);
                    var check = _context.Users.Where(x => x.Id == handoverLeave.EmployeeId).Select(s => new { s.Email }).FirstOrDefault();
                    var samps = check.Email;

                    var message = new MimeMessage();
                    //Setting the To e-mail address
                    message.To.Add(new MailboxAddress(samps));


                    var tt = "akinbamidelea@novohealthafrica.org";
                    message.Bcc.Add(new MailboxAddress(tt));
                    //Setting the From e-mail address
                    message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                    //E-mail subject 
                    message.Subject = "Handover Leaves";
                    //E-mail message body
                    message.Body = new TextPart(TextFormat.Html)
                    {
                        Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                       "<br/><br/><br/><br/>" +
                    "<p>Dear Colleague,</p>" +

                   "<p> This is to notify that handover has been made by " + user.FirstName + " " + user.LastName +

                   "<br/>on </a>" + handoverLeave.CreateDate + ".</p>" +
                   "You can see more information on the handover by Clicking <a href='https://hrms.novohealthafrica.org/HandoverLeaves1/Details/" + handoverLeave.Id + "'>Here</a>" +




                                                  "<br/>Regards<br/>" +


                                                   "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"
                    };

                    //Configure the e-mail
                    using (var emailClient = new SmtpClient())
                    {

                        emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                        emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                        emailClient.Connect("smtp.office365.com", 587, false);
                        emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2009aa");
                        emailClient.Send(message);
                        emailClient.Disconnect(true);
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HandoverLeaveExists(handoverLeave.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EmployeeId"] = new SelectList(_context.Users, "Id", "FirstName", handoverLeave.EmployeeId);
            return View(handoverLeave);
        }

        // GET: HandoverLeaves1/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var handoverLeave = await _context.HandoverLeaves
                .Include(h => h.GetUser)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (handoverLeave == null)
            {
                return NotFound();
            }

            return View(handoverLeave);
        }

        // POST: HandoverLeaves1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var handoverLeave = await _context.HandoverLeaves.FindAsync(id);
            _context.HandoverLeaves.Remove(handoverLeave);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HandoverLeaveExists(int id)
        {
            return _context.HandoverLeaves.Any(e => e.Id == id);
        }
    }
}
