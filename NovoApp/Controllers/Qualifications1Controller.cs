﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Novo.Models.Entity;
using Novo.Models.ViewModels;
using NovoClients.DataAccess.Data;

namespace NovoApp.Controllers
{
    public class Qualifications1Controller : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public Qualifications1Controller(ApplicationDbContext context,
                                                SignInManager<ApplicationUser> signInManager,
                                                UserManager<ApplicationUser> userManager,
                                                IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _signInManager = signInManager;
            _userManager = userManager;
        }

        // GET: Qualifications1
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            //return RedirectToAction("Details", "Qualifications");
            return View(await _context.NewQualifications.Where(x => x.EmployeeId == user.Id).ToListAsync());
        }

        // GET: Qualifications1/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var user = await _userManager.GetUserAsync(User);

            var qualification = await _context.NewQualifications.Where(x => x.Id == id)
                .FirstOrDefaultAsync();
            if (qualification == null)
            {
                return NotFound();
            }

            return View(qualification);
        }

        // GET: Qualifications1/Create
        public IActionResult Create()
        {
            var model = new QualificationVm();
            return View(model);
        }

        // POST: Qualifications1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(QualificationVm qualifications)
        {
            if (ModelState.IsValid || !ModelState.IsValid)
            {
                string uniqueFileName = null;

                if (qualifications.Image != null && qualifications.Image.Length > 0)
                {

                    string uploadFolder = Path.Combine(_hostingEnvironment.WebRootPath, "images/Cert");
                    uniqueFileName = Guid.NewGuid().ToString() + "_" + qualifications.Image.FileName;
                    string filePath = Path.Combine(uploadFolder, uniqueFileName);
                    qualifications.Image.CopyTo(new FileStream(filePath, FileMode.Create));

                }

                var model = new Qualifications
                {
                    EmployeeId = qualifications.EmployeeId,
                    Academic=qualifications.Academic,
                    AcademicDescription=qualifications.AcademicDescription,
                    Professional=qualifications.Professional,
                    ProfessionalDescription=qualifications.ProfessionalDescription,
                    Institution=qualifications.Institution,
                    Duration=qualifications.Duration,
                    StartDate=qualifications.StartDate,
                    EndDate=qualifications.EndDate,
                    Image=uniqueFileName,
                    AddedBy=qualifications.AddedBy,
                    CreateDate=qualifications.CreateDate
                    
                };
                _context.Add(model);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(qualifications);
        }

        // GET: Qualifications1/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var qualifications = await _context.NewQualifications.FindAsync(id);
            if (qualifications == null)
            {
                return NotFound();
            }

            var model = new QualificationEditVm()
            {
                EmployeeId = qualifications.EmployeeId,
                Academic = qualifications.Academic,
                AcademicDescription = qualifications.AcademicDescription,
                Professional = qualifications.Professional,
                ProfessionalDescription = qualifications.ProfessionalDescription,
                Institution = qualifications.Institution,
                Duration = qualifications.Duration,
                StartDate = qualifications.StartDate,
                EndDate = qualifications.EndDate,
                AddedBy = qualifications.AddedBy,
                CreateDate = qualifications.CreateDate,
                ExistingPhotoPath=qualifications.Image

            };

            return View(model);
        }

        // POST: Qualifications1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(QualificationEditVm model)
        {

            if (ModelState.IsValid || !ModelState.IsValid)
            {
                var userfind = await _context.NewQualifications.FindAsync(model.Id);
                if (userfind == null)
                {
                    return NotFound();
                }

                userfind.EmployeeId = model.EmployeeId;
                userfind.Academic = model.Academic;
                userfind.AcademicDescription = model.AcademicDescription;
                userfind.Professional = model.Professional;
                userfind.ProfessionalDescription = model.ProfessionalDescription;
                userfind.Institution = model.Institution;
                userfind.Duration = model.Duration;
                userfind.StartDate = model.StartDate;
                userfind.EndDate = model.EndDate;
                userfind.AddedBy = model.AddedBy;
                userfind.CreateDate = model.CreateDate;

                string uniqueFileName = null;

                if (model.Image != null && model.Image.Length > 0)
                {
                    if (model.ExistingPhotoPath != null)
                    {
                        string filepath = Path.Combine(_hostingEnvironment.WebRootPath, "images", model.ExistingPhotoPath);
                        System.IO.File.Delete(filepath);
                    }
                    string uploadFolder = Path.Combine(_hostingEnvironment.WebRootPath, "images/Cert");
                    uniqueFileName = Guid.NewGuid().ToString() + "_" + model.Image.FileName;
                    string filePath = Path.Combine(uploadFolder, uniqueFileName);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        model.Image.CopyTo(fileStream);
                    }

                    userfind.Image= uniqueFileName;
                }
                _context.Update(userfind);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        // GET: Qualifications1/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var qualifications = await _context.NewQualifications
                .FirstOrDefaultAsync(m => m.Id == id);
            if (qualifications == null)
            {
                return NotFound();
            }

            return View(qualifications);
        }

        // POST: Qualifications1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var qualifications = await _context.NewQualifications.FindAsync(id);
            _context.NewQualifications.Remove(qualifications);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool QualificationsExists(int id)
        {
            return _context.NewQualifications.Any(e => e.Id == id);
        }
    }
}
