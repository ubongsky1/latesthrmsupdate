﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Novo.Models.Entity;
using NovoClients.DataAccess.Data;

namespace NovoApp.Controllers
{
    public class EmployeeContacts1Controller : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public EmployeeContacts1Controller(ApplicationDbContext context, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        // GET: EmployeeContacts1
        public async Task<IActionResult> Index()
        {
            return View(await _context.GetEmpContacts.ToListAsync());
        }

        public async Task<IActionResult> Details()
        {
            var user = await _userManager.GetUserAsync(User);

            var employeeContact = await _context.GetEmpContacts.FirstOrDefaultAsync(x => x.EmployeeId == user.Id);
                
            if (employeeContact == null)
            {
                return RedirectToAction("Create", "EmployeeContacts1");

            }

            return View(employeeContact);
        }

        // GET: EmployeeContacts1/Details/5
        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeeContacts = await _context.GetEmpContacts
                .FirstOrDefaultAsync(m => m.Id == id);
            if (employeeContacts == null)
            {
                return NotFound();
            }

            return View(employeeContacts);
        }

        // GET: EmployeeContacts1/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: EmployeeContacts1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(EmployeeContacts employeeContacts)
        {
            if (ModelState.IsValid || !ModelState.IsValid)
            {
                _context.Add(employeeContacts);
                await _context.SaveChangesAsync();
                return RedirectToAction("Create", "Qualifications1");
            }
            return View(employeeContacts);
        }

        // GET: EmployeeContacts1/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeeContacts = await _context.GetEmpContacts.FindAsync(id);
            if (employeeContacts == null)
            {
                return NotFound();
            }
            return View(employeeContacts);
        }

        // POST: EmployeeContacts1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("EmployeeId,Address,City,StateOfOrigin,CountryOfOrigin,BeneficiaryName,BeneficiaryNo,BeneficiaryAddress,EmergencyName,EmergencyNo,EmergencyAddress,Lgas,States,Region,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete")] EmployeeContacts employeeContacts)
        {
            if (id != employeeContacts.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(employeeContacts);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmployeeContactsExists(employeeContacts.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(employeeContacts);
        }

        // GET: EmployeeContacts1/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeeContacts = await _context.GetEmpContacts
                .FirstOrDefaultAsync(m => m.Id == id);
            if (employeeContacts == null)
            {
                return NotFound();
            }

            return View(employeeContacts);
        }

        // POST: EmployeeContacts1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var employeeContacts = await _context.GetEmpContacts.FindAsync(id);
            _context.GetEmpContacts.Remove(employeeContacts);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmployeeContactsExists(int id)
        {
            return _context.GetEmpContacts.Any(e => e.Id == id);
        }
    }
}
