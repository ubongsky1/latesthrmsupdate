﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Novo.Models.Entity;
using NovoApp.Moddel;
using NovoApp.Utility;
using NovoClients.DataAccess.Data;

namespace NovoApp.Controllers
{
    public class EmploymentDetailsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        [BindProperty]
        public EmployeeDetailVM EmpVM { get; set; }

        public EmploymentDetailsController(ApplicationDbContext context,
                                                SignInManager<ApplicationUser> signInManager,
                                                UserManager<ApplicationUser> userManager,
                                                IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _signInManager = signInManager;
            _userManager = userManager;
            EmpVM = new EmployeeDetailVM()
            {
               
                EmployeementDetails = new Novo.Models.Entity.EmploymentDetails()
            };
        }

        // GET: EmploymentDetails1
        public async Task<IActionResult> Index()
        {
            return View(await _context.EmploymentDetails.ToListAsync());
        }


        public async Task<IActionResult> Details()
        {
            var user = await _userManager.GetUserAsync(User);
            //ViewBag.Unit = _context.Units.Where(x => x.Id == user.Id).Select(x => x.Name);
           

            ViewBag.Unit = _context.Units.Where(x => x.Id == user.UnitsId).Select(x => x.Name).FirstOrDefault();

            ViewBag.Leaves = _context.Leaves.Where(x => x.EmployeeId == user.Id).Select(x => x.TotalLeave).LastOrDefault();
            ViewBag.Utilized = _context.Leaves.Where(x => x.EmployeeEmail == user.Email).Count();
            ViewBag.Remaining = ViewBag.Leaves - ViewBag.Utilized;
            //ViewBag.Remaining = _context.Leaves.Where(x => x.EmployeeId == user.Id).Select(x => x.LeaveRemaining).LastOrDefault();

            var employeedetails = _context.EmploymentDetails.Where(x => x.EmployeeId == user.Id)
               // .Include(e => e.Units)
                .FirstOrDefault();

            if (employeedetails == null)
            {
                return RedirectToAction("Create", "EmploymentDetails");
            }

            return View(employeedetails);
        }

        // GET: EmploymentDetails1/Details/5 ------ For HR
        public async Task<IActionResult> Detailss(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var employmentDetail = await _context.EmploymentDetails.FirstOrDefaultAsync(m => m.Id == id);
            var use = employmentDetail.EmployeeId;
            var user = await _userManager.FindByIdAsync(use.ToString());

            ViewBag.Unit = _context.Units.Where(x => x.Id == user.UnitsId).Select(x => x.Name).FirstOrDefault();
            
           // var use = _context.EmploymentDetails.Where();

            ViewBag.Leaves = _context.Leaves.Where(x => x.EmployeeId == employmentDetail.EmployeeId).Select(x => x.TotalLeave).LastOrDefault();
            ViewBag.Utilized = _context.Leaves.Where(x => x.EmployeeEmail == employmentDetail.Email).Count();
            ViewBag.Remaining = ViewBag.Leaves - ViewBag.Utilized;
            //ViewBag.Remaining = _context.Leaves.Where(x => x.EmployeeId == employmentDetail.EmployeeId).Select(x => x.LeaveRemaining).LastOrDefault();

            var employmentDetails = await _context.EmploymentDetails.FirstOrDefaultAsync(m => m.Id == id);
            if (employmentDetails == null)
            {
                return RedirectToAction("Create", "EmploymentDetails");
            }

            return View(employmentDetails);

        }


        // GET: EmploymentDetails1/Create
        public IActionResult Create()
        {
            

            return View();
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(EmploymentDetails employmentDetails)
        {
            //ViewBag.Units = new SelectList(_context.Units, "Id", "Name");
            //ViewData["UnitsId"] = new SelectList(_context.Units, "Id", "Name");
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Create", "EmploymentDetails");
                //return View(EmpVM);
            }

            _context.EmploymentDetails.Add(employmentDetails);
            await _context.SaveChangesAsync();

            //Image being saved

            string webRootPath = _hostingEnvironment.WebRootPath;
            var files = HttpContext.Request.Form.Files;

            var employeeFromDb = _context.EmploymentDetails.Find(employmentDetails.Id);
            
            if (files.Count != 0)
            {
               
                var code = Guid.NewGuid().ToString("n").Substring(0, 4);
                //Image has been uploaded
                var uploads = Path.Combine(webRootPath, SD.ImageFolder);
                var extension = Path.GetExtension(files[0].FileName);
                

                using (var filestream = new FileStream(Path.Combine(uploads, employmentDetails.Id + extension), FileMode.Create))
                {
                    files[0].CopyTo(filestream);
                }
                employeeFromDb.Image = @"\" + SD.ImageFolder + @"\" + employmentDetails.Id + extension;
            }
            else
            {
                var code = Guid.NewGuid().ToString("n").Substring(0, 4);
                //when user does not upload image
                var uploads = Path.Combine(webRootPath, SD.ImageFolder + @"\" + SD.DefaultProductImage);
                System.IO.File.Copy(uploads, webRootPath + @"\" + SD.ImageFolder + @"\" + code + employmentDetails.Id + ".png");
                employeeFromDb.Image = @"\" + SD.ImageFolder + @"\" + code + employmentDetails.Id + ".png";
            }
           
            await _context.SaveChangesAsync();
            //return Redirect("~/EmployeeContacts/Create");
            return RedirectToAction("Create", "EmployeeContacts");

        }



        // GET: EmploymentDetails1/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {

            
            if (id == null)
            {
                return NotFound();
            }

            var employmentDetails = await _context.EmploymentDetails.FindAsync(id);
            if (employmentDetails == null)
            {
                return NotFound();
            }
            //ViewData["UnitsId"] = new SelectList(_context.Units, "Id", "Name", EmpVM.EmployeementDetails.UnitsId);

            return View(employmentDetails);
        }

        // POST: EmploymentDetails1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("EmployeeId,Fullname,BloodGroup,DOB,Gender,MaritalStatus,Designation,EmployeeNo,Email,ModeOfEmployment,UnitId,JobTitle,EntryLevel,IsUnitHead,IsDeptHead,IsRegionalHead,CurrentLevel,LastPromotion,DateOfEmployment,DateOfLeaving,YearsOfExperience,Image,CUG,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete")] EmploymentDetails employmentDetails)
        {
           

            if (id != employmentDetails.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(employmentDetails);
                    string webRootPath = _hostingEnvironment.WebRootPath;
                    var files = HttpContext.Request.Form.Files;

                    var productFromDb = _context.EmploymentDetails.Where(m => m.Id == employmentDetails.Id).FirstOrDefault();

                    if (files.Count > 0 && files[0] != null)
                    {
                        //if user uploads a new image
                        var uploads = Path.Combine(webRootPath, SD.ImageFolder);
                        var extension_new = Path.GetExtension(files[0].FileName);
                        var extension_old = Path.GetExtension(productFromDb.Image);

                        if (System.IO.File.Exists(Path.Combine(uploads, employmentDetails.Id + extension_old)))
                        {
                            System.IO.File.Delete(Path.Combine(uploads, employmentDetails.Id + extension_old));
                        }
                        using (var filestream = new FileStream(Path.Combine(uploads, employmentDetails.Id + extension_new), FileMode.Create))
                        {
                            files[0].CopyTo(filestream);
                        }
                        employmentDetails.Image = @"\" + SD.ImageFolder + @"\" + employmentDetails.Id + extension_new;
                    }
                    else
                    {
                        var code = Guid.NewGuid().ToString("n").Substring(0, 4);
                        //when user does not upload image
                        var uploads = Path.Combine(webRootPath, SD.ImageFolder + @"\" + SD.DefaultProductImage);
                        System.IO.File.Copy(uploads, webRootPath + @"\" + SD.ImageFolder + @"\" + code + employmentDetails.Id + ".png");
                        employmentDetails.Image = @"\" + SD.ImageFolder + @"\" + code + employmentDetails.Id + ".png";
                    }

                    if (employmentDetails.Image != null)
                    {
                        productFromDb.Image = employmentDetails.Image;
                    }
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmploymentDetailsExists(employmentDetails.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", "EmploymentDetails1");
            }
            //ViewData["UnitsId"] = new SelectList(_context.Units, "Id", "Name", EmpVM.EmployeementDetails.UnitsId);

            return View(employmentDetails);
        }

        // GET: EmploymentDetails1/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employmentDetails = await _context.EmploymentDetails
                //.Include(e => e.Units)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (employmentDetails == null)
            {
                return NotFound();
            }

            return View(employmentDetails);
        }

        // POST: EmploymentDetails1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var employmentDetails = await _context.EmploymentDetails.FindAsync(id);
            _context.EmploymentDetails.Remove(employmentDetails);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmploymentDetailsExists(int id)
        {
            return _context.EmploymentDetails.Any(e => e.Id == id);
        }
    }
}
