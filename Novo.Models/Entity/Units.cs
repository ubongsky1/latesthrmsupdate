﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Novo.Models.Entity
{
    public class Units : Base
    {

        public string Name { get; set; }

        [Required]
        [Display(Name = "Department")]
        public string Departments { get; set; }

    }
}
