﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Novo.Models.Entity
{
    public class Appraisal : Base
    {
        public int EmployeeId { get; set; }
        public string EmployeeEmail { get; set; }
        public int OperationalExpensesReduction { get; set; }
        public int OperationalExpensesReductionAppraisal { get; set; }
        public int MedicalBillsReduction { get; set; }
        public int MedicalBillsReductionAppraisal { get; set; }
        public int RetailProduct { get; set; }
        public int RetailProductAppraisal { get; set; }
        public int FinancialSubTotal { get; set; }
        public int FinancialSubTotalAppraisal { get; set; }
        public int TurnAroundTime { get; set; }
        public int TurnAroundTimeAppraisal { get; set; }
        public int MinimalComplaints { get; set; }
        public int MinimalComplaintsAppraisal { get; set; }
        public int ConstantCommunication { get; set; }
        public int ConstantCommunicationAppraisal { get; set; }
        public int CustomerSatisfactionTotal { get; set; }
        public int CustomerSatisfactionTotalAppraisal { get; set; }
        public int TimelyCompletion { get; set; }
        public int TimelyCompletionAppraisal { get; set; }
        public int TimelyAudit { get; set; }
        public int TimelyAuditAppraisal { get; set; }
        public int ZeroNonCompliance { get; set; }
        public int ZeroNonComplianceAppraisal { get; set; }
        public int PromptReview { get; set; }
        public int PromptReviewAppraisal { get; set; }
        public int ZeroProcessFailures { get; set; }
        public int ZeroProcessFailuresAppraisal { get; set; }
        public int InternalProcess { get; set; }
        public int InternalProcessAppraisal { get; set; }
        public int AdhocTasks { get; set; }
        public int AdhocTasksAppraisal { get; set; }
        public int ProcessTotal { get; set; }
        public int ProcessTotalAppraisal { get; set; }
        public int TrainingHours { get; set; }
        public int TrainingHoursAppraisal { get; set; }
        public int DisciplinaryRecord { get; set; }
        public int DisciplinaryRecordAppraisal { get; set; }
        public int Culture { get; set; }
        public int CultureAppraisal { get; set; }
        public int AttendanceAndPunctuality { get; set; }
        public int AttendanceAndPunctualityAppraisal { get; set; }
        public int TechnologyProficiency { get; set; }
        public int TechnologyProficiencyAppraisal { get; set; }
        public int LearningTotal { get; set; }
        public int LearningTotalAppraisal { get; set; }
        public int Teamwork { get; set; }
        public int TeamworkAppraisal { get; set; }
        public int Professionalism { get; set; }
        public int ProfessionalismAppraisal { get; set; }
        public int CostConciousness { get; set; }
        public int CostConciousnessAppraisal { get; set; }
        public int Communication { get; set; }
        public int CommunicationAppraisal { get; set; }
        public int PersonalDiscipline { get; set; }
        public int PersonalDisciplineAppraisal { get; set; }
        public int CustomerService { get; set; }
        public int CustomerServiceAppraisal { get; set; }
        public int Innovation { get; set; }
        public int InnovationAppraisal { get; set; }
        public int ManagementSkills { get; set; }
        public int ManagementSkillsAppraisal { get; set; }
        public int ContinuousImprovement { get; set; }
        public int ContinuousImprovementAppraisal { get; set; }
        public int MeetingDeadlines { get; set; }
        public int MeetingDeadlinesAppraisal { get; set; }
        public int EmployeeBTotalScore { get; set; }
        public int AppraiserBTotalScore { get; set; }
        public int ExceptionalAchievements { get; set; }
        public int SectionAScore { get; set; }
        public int SectionAScoreAppraisal { get; set; }
        public double SectionBTotal { get; set; }
        public double FinalScore { get; set; }
        public string Grade { get; set; }
        [Required]
        [Display(Name = "Employee Unit")]
        public int UnitsId { get; set; }
        public int Question7 { get; set; }
        public int Question7Appraisail { get; set; }
        public int Question8 { get; set; }
        public int Question8Appraisal { get; set; }
    }
}
