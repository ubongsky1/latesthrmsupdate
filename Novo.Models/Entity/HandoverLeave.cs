﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Novo.Models.Entity
{
   public class HandoverLeave : Base
    {
        
        [Display(Name = "Employee")]
        public string Employee { get; set; }
        [Required]
        [Display(Name = "Leave Handover Note")]
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }
        [ForeignKey("GetUser")]
        [Display(Name ="Assignee")]
        public int EmployeeId { get; set; }
        public virtual ApplicationUser GetUser { get; set; }
    }
}
