﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Novo.Models.Entity
{
    public class Form : Base
    {
        public int EmployeeId { get; set; }
        public string Question1 { get; set; }
        public int Weight1 { get; set; }
        public string Question2 { get; set; }
        public int Weight2 { get; set; }
        public string Question3 { get; set; }
        public int Weight3 { get; set; }
        public string Question4 { get; set; }
        public int Weight4 { get; set; }
        public string Question5 { get; set; }
        public int Weight5 { get; set; }
        public string Question6 { get; set; }
        public int Weight6 { get; set; }
        public string Question7 { get; set; }
        public int Weight7 { get; set; }
        public string Question8 { get; set; }
        public int Weight8 { get; set; }
        public string Question9 { get; set; }
        public int Weight9 { get; set; }
        public string Question10 { get; set; }
        public int Weight10 { get; set; }
        public string Question11 { get; set; }
        public int Weight11 { get; set; }
        public string Question12 { get; set; }
        public int Weight12 { get; set; }
        public string Question13 { get; set; }
        public int Weight13 { get; set; }
        public string Question14 { get; set; }
        public int Weight14 { get; set; }
        public string Question15 { get; set; }
        public int Weight15 { get; set; }
    }
}
