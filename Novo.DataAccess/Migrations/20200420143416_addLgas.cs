﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class addLgas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Lgas",
                table: "EmployeeContacts",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Lgas",
                table: "EmployeeContacts");
        }
    }
}
