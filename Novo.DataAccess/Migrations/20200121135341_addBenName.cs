﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class addBenName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BeneficiaryName",
                table: "EmployeeContacts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EmergencyName",
                table: "EmployeeContacts",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BeneficiaryName",
                table: "EmployeeContacts");

            migrationBuilder.DropColumn(
                name: "EmergencyName",
                table: "EmployeeContacts");
        }
    }
}
