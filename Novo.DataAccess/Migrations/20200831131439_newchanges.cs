﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class newchanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "currentTasks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AddedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false),
                    TaskName = table.Column<string>(nullable: true),
                    TaskType = table.Column<string>(nullable: true),
                    TaskDescription = table.Column<string>(nullable: true),
                    AssignBy = table.Column<string>(nullable: true),
                    EmployeeId = table.Column<int>(nullable: false),
                    ExpectedStartDate = table.Column<string>(nullable: true),
                    ExpectedEndDate = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    Comment = table.Column<string>(nullable: true),
                    Remark = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    isUnitHeadApproved = table.Column<bool>(nullable: false),
                    isHrApproved = table.Column<bool>(nullable: false),
                    isEmpApproved = table.Column<bool>(nullable: false),
                    isUnitHeadDisapprove = table.Column<bool>(nullable: false),
                    isHrDisapprove = table.Column<bool>(nullable: false),
                    AssignToId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_currentTasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_currentTasks_Employee_AssignToId",
                        column: x => x.AssignToId,
                        principalTable: "Employee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LatestTasks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AddedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false),
                    TaskName = table.Column<string>(nullable: true),
                    TaskType = table.Column<string>(nullable: true),
                    TaskDescription = table.Column<string>(nullable: true),
                    AssignBy = table.Column<string>(nullable: true),
                    EmployeeId = table.Column<int>(nullable: false),
                    ExpectedStartDate = table.Column<string>(nullable: true),
                    ExpectedEndDate = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    Comment = table.Column<string>(nullable: true),
                    Remark = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    isUnitHeadApproved = table.Column<bool>(nullable: false),
                    isHrApproved = table.Column<bool>(nullable: false),
                    isEmpApproved = table.Column<bool>(nullable: false),
                    isUnitHeadDisapprove = table.Column<bool>(nullable: false),
                    isHrDisapprove = table.Column<bool>(nullable: false),
                    AssignToId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LatestTasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LatestTasks_Employee_AssignToId",
                        column: x => x.AssignToId,
                        principalTable: "Employee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_currentTasks_AssignToId",
                table: "currentTasks",
                column: "AssignToId");

            migrationBuilder.CreateIndex(
                name: "IX_LatestTasks_AssignToId",
                table: "LatestTasks",
                column: "AssignToId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "currentTasks");

            migrationBuilder.DropTable(
                name: "LatestTasks");
        }
    }
}
