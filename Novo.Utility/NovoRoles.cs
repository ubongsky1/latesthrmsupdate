﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NovoClients.Utility
{
    public static class NovoRoles
    {
        public const string SuperAdmin = "Super Admin";
        public const string Admin = "Admin";
        public const string User = "User";
        public const string HRHead = "HR Head";
        public const string HR = "HR";
        public const string UnitHead = "Unit Head";
        public const string DeptHead = "Dept Head";
        public const string RegionalHead = "Regional Head";

    }
}
